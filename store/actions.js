export default {
  async getArticle({ commit, state}, term) {
    let article = state.articles.map(item => {    
      if (term == item.url) {
        return this.article = item
      }
    })
    commit('setArticle', article.filter(Boolean)[0])
  }
}
