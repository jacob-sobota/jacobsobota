const pkg = require('./package')

module.exports = {
  mode: 'universal',
  head: {
    htmlAttrs: {
      lang: 'pl',
    },
    title: 'Jacob Sobota - tworzenie mobilnych stron internetowych z optymalizacją SEO',
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        name: 'description',
        content: 'Tworzenie mobilnych stron www wraz z optymalizacją SEO. Strony dostosowywane do potrzeb klienta, bez wciskania kitu! - Jacob Sobota'
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      }
    ],
    script: [
      {
        type: 'text/javascript',
        src: '/smtp.js'
      }
    ]
  },
  loading: {
    color: '#fff'
  },
  css: ['~assets/scss/style.scss'],
  modules: [
    '@nuxtjs/axios',
    'nuxt-fontawesome',
    ['@nuxtjs/google-analytics', { id: 'UA-73833712-1' }]
  ],
  fontawesome: {
    imports: [
      {
        set: '@fortawesome/free-solid-svg-icons',
        icons: ['faCode', 'faHeart', 'faTimes']
      },
      {
        set: '@fortawesome/free-brands-svg-icons',
        icons: ['faVuejs']
      }
    ]
  },
  router: {
    linkActiveClass: 'active-link'
  },
  build: {
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
